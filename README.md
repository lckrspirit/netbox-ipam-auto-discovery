# Netbox-ipam-auto-discovery

Скриптец, который проверем доступность хоста и добавляем/удаляем его из IPAM в Netbox. 

Предварительно, нужно сгенерить api токен в netbox. Далее редактируем скриптец правим эти переменные:
```
# General vars
netbox_url = '<NETBOX_API_SERVER>'
netbox_api = '<NETBOX_API_TOKEN'
```

Ставим зависимости:
```
pip3 install -r requirements.txt
```

Запускаем так:
```
D:\Scripts\Netbox> python .\netbox_scan.py 192.168.150.0/24
```
